'use strict';
/**
 * Created by Dominic on 15-Sep-2014.
 */
var mongoose = require('mongoose'),
    DiagnosisSchema = require('./diagnosis'),
    Schema = mongoose.Schema ;

var ChildSchema = mongoose.Schema({
    first_name: {
        type: String,
        required:'First name of child is required',
        trim:true
    },
    surname: {
        type: String,
        required:'surname of child is required',
        trim:true
    },
    other_names: String,
    gender: {type:String, enum : ['Male', 'Female'],  required:'child gender is required'},
    birthDate : {
        type: Date,
        required:'date of birth is required'
    },
    address:{
        type: String,
        required:'contact address of child is required',
        trim:true
    },
    location_info :{
        locality_name:String,
        location_id:{type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Location'},
        primaryLocation_id :{type: mongoose.Schema.Types.ObjectId, required: true}
    },
    birth_clinic_hospital:String,
    birth_weight: {
        type: Number,
        min:0,
        max:10
    },
    primary_notification_email: String,

    primary_notification_phone: String,

    parentInformation: [
        {
            name: String,
            parentContactNos: [{
                numberType:String,
                number:String
            }],
            contact_email: String,
            address: String,
            remarks: String
        }],

    birthInfections : [{
        infections: [String]
    }],

    vaccinations: [{
        vaccine_inoculation:String,
        is_administered:Boolean
    }],

    renown_infections:[String],

    diagnosis:[DiagnosisSchema],

    infectionManagement:[{
        managementFacility:[ {
            facilityName:String,
            facilityId: String,
            isPrimary: Boolean

        }]
    }],

    created_by: {
        type: Schema.ObjectId,
        ref: 'Account',
        required:'Account Identity is required'
    },

    required_vaccinations:[String]


});

var Child = mongoose.model('Child', ChildSchema);

module.exports = Child;
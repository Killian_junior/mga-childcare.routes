/**
 * Created by Dominic on 11-Oct-2014.
 */
    
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema ;

var VaccineSchema = mongoose.Schema({
    vaccine_name:{
        type:String,
        trim:true,
        required: 'Please enter vaccine name'
    },
    combat_disease:{
        type:String,
        trim:true
    },
    required_time:{
        type:Number,
        required: 'Please enter time vaccine administration'
    },
    required_time_type:{
        type:String,
        required: 'Please enter time type',
        enum: ['Days', 'Weeks', 'Months']
    },
    remarks:{
        type:String,
        trim:true
    },

    created_by: {
        type: Schema.ObjectId,
        ref: 'Account',
        required:'Account Identity is required'
    },
    discontinue : Boolean,
    reason_for_discontinue :{
        type:String,
        trim:true
    }
});

var Vaccine = mongoose.model('Vaccine', VaccineSchema);

module.exports = Vaccine;
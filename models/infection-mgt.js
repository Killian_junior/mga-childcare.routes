/**
 * Created by Dominic on 11-Oct-2014.
 */
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema ;

var infection_managementSchema = mongoose.Schema({
    medication:{
        type:String,
        trim:true
    },
    duration:{
        type:String,
        trim:true
    },
    administered:Boolean,
    dose:{
        type:String,
        trim:true
    },
    remarks:{
        type:String,
        trim:true
    },
    inception_date:{
        type:Date,
        required: 'Please enter evaluation date'
    }

});

var InfectionSchema = mongoose.Schema({
    infection_details:{
        type:String,
        trim:true
    },
    remarks:{
        type:String,
        trim:true
    },
    identification_date:{
        type:Date,
        required: 'Please enter evaluation date'
    },
    primary_examiner: String,
    examiners : [String],
    management:[infection_managementSchema],
    child: {
        type: Schema.ObjectId,
        ref: 'Child',
        required:'Child Identity is required'
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'Account',
        required:'Account Identity is required'
    },

    infection_classification : {
        type:String,
        trim:true,
        enum: ['Mild', 'Moderate', 'Severe', 'Critical']
    }
});


var Infection = mongoose.model('Infection', InfectionSchema);

module.exports = Infection;

/**
 * Created by Dominic on 11-Oct-2014.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema ;

var treatmentSchema = mongoose.Schema({

    medication:{
        type:String,
        trim:true
    },
    duration:{
        type:String,
        trim:true
    },
    administered:Boolean,
    dose:{
        type:String,
        trim:true
    },
    remarks:{
        type:String,
        trim:true
    },
    inception_date:{
        type:Date,
        required: 'Please enter evaluation date'
    }

});

var DiagnosisSchema = mongoose.Schema({
    evaluation_details:{
        type:String,
        trim:true
    },
    remarks:{
        type:String,
        trim:true
    },
    evaluation_date:{
        type:Date,
        required: 'Please enter evaluation date'
    },
    primary_examiner: String,
    examiners : [String],
    treatment:[treatmentSchema],
    child: {
        type: Schema.ObjectId,
        ref: 'Child',
        required:'Child Identity is required'
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'Account',
        required:'Account Identity is required'
    }
});


var Diagnosis = mongoose.model('Diagnosis', DiagnosisSchema);

module.exports = Diagnosis;
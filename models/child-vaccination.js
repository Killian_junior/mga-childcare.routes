'use strict';
/**
 * Created by Dominic on 10-Sep-2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema ;



var ChildImmunizationSchema = mongoose.Schema({
    vaccine:{
        type:Schema.ObjectId,
        ref: 'Vaccine',
        required: 'Please enter vaccine identity'
    },
    administration_date:{
        type:Date,
        required: 'Please enter time vaccine administration'
    },
    remarks:{
        type:String,
        trim:true
    },
    administration_by:{
        type:String,
        trim:true
    },
    child: {
        type: Schema.ObjectId,
        ref: 'Child',
        required:'Child Identity is required'
    },
    created_by: {
        type: Schema.ObjectId,
        ref: 'Account',
        required:'Account Identity is required'
    }
});

var ChildImmunization = mongoose.model('ChildImmunization', ChildImmunizationSchema);

module.exports = ChildImmunization;

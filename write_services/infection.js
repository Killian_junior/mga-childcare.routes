/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var Infection = require('../models/infection-mgt');

_ = require('lodash');

exports.createInfection = function(args, next) {
    var obj = new Infection(args);

    obj.save(function(err, result){
        if(err){
            return next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result);
        }
    })

};

exports.updateInfection = function(args, next) {

    Infection.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeInfectionById = function(infectionId, next) {

    Infection.findById(infectionId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};
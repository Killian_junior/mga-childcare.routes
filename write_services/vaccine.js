/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var Vaccine = require('../models/vaccine');

_ = require('lodash');

exports.createVaccine = function(args, next) {
    var obj = new Vaccine(args);

    obj.save(function(err, result){
        if(err){
            return next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result);
        }
    })

};

exports.updateVaccine = function(args, next) {

    Vaccine.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeVaccineById = function(vaccineId, next) {

    Vaccine.findById(vaccineId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};
/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var childVaccination = require('../models/child-vaccination');

_ = require('lodash');

exports.createChildVaccination = function(args, next) {
    var obj = new childVaccination(args);

    obj.save(function(err, result){
        if(err){
            return next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result);
        }
    })

};

exports.updateChildVaccination = function(args, next) {

    childVaccination.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeChildVaccinationById = function(childVaccinationId, next) {

    childVaccination.findById(childVaccinationId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};
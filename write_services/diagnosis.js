/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var diagnosis = require('../models/diagnosis');

_ = require('lodash');

exports.createDiagnosis = function(args, next) {
    var obj = new diagnosis(args);

    obj.save(function(err, result){
        if(err){
            return next(err, null);
        }
        if(result){
            console.log(result);
            return next(null, result);
        }
    })

};

exports.updateDiagnosis = function(args, next) {

    diagnosis.findById(args._id,
        function(err, doc){
            if(err){
                return  next(err, null);
            }
            if(doc){
                doc = _.extend(doc, args);

                doc.save(function(err){
                    if(!err){
                        return  next(null, true);
                    }
                });

            }
        });

};

exports.removeDiagnosisById = function(diagnosisId, next) {

    diagnosis.findById(diagnosisId,
        function (err, doc) {
            if (err) {
                return  next(err, null);
            }
            if (doc) {
                doc.remove(function (err) {
                    if (err) {
                        return next(err, null);
                    } else {
                        return next(null, doc);
                    }
                })

            }
        });

};
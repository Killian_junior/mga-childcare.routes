/**
 * Created by TERMINAL 7 on 10/20/2014.
 */


var Diagnosis = require('../models/diagnosis');

exports.getDiagnosis = function(diagnosisId, next) {

    Diagnosis.findById({_id:diagnosisId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getAllDiagnosis = function(args, next) {

    Diagnosis.find({},
        function(err, result){
            if(err){
                return  next(err, null);
            }
            if(result){
                return  next(null, result);
            }
        });

};


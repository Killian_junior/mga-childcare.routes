/**
 * Created by TERMINAL 7 on 10/20/2014.
 */


var childVaccination = require('../models/child-vaccination');

exports.getChildVaccination = function(childVaccinationId, next) {

    childVaccination.findById({_id:childVaccinationId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getChildVaccinations = function(args, next) {

    childVaccination.find({},
        function(err, result){
            if(err){
                return  next(err, null);
            }
            if(result){
                return  next(null, result);
            }
        });

};


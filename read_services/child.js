/**
 * Created by TERMINAL 7 on 10/20/2014.
 */


var child = require('../models/child');

exports.getChild = function(childId, next) {

    child.findById({_id:childId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getChildren = function(args, next) {

    child.find({},
        function(err, result){
            if(err){
                return  next(err, null);
            }
            if(result){
                return  next(null, result);
            }
        });

};


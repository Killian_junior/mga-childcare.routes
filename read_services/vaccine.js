/**
 * Created by TERMINAL 7 on 10/20/2014.
 */


var Vaccine = require('../models/vaccine');

exports.getVaccine = function(vaccineId, next) {

    Vaccine.findById({_id:vaccineId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getVaccines = function(args, next) {

    Vaccine.find({},
        function(err, result){
            if(err){
                return  next(err, null);
            }
            if(result){
                return  next(null, result);
            }
        });

};


/**
 * Created by TERMINAL 7 on 10/20/2014.
 */


var Infection = require('../models/infection-mgt');

exports.getInfection = function(infectionId, next) {

    Infection.findById({_id:infectionId._id}, function(err, result){
        if(err){
            return  next(err, null);
        }
        if(result){
            return  next(null, result);
        }
    });

};

exports.getInfections = function(args, next) {

    Infection.find({},
        function(err, result){
            if(err){
                return  next(err, null);
            }
            if(result){
                return  next(null, result);
            }
        });

};


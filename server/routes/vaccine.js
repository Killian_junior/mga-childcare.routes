/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var vaccine = require('../requesthandlers/child-care/child-care');

module.exports = function(app){

    app.route('/vaccine/create').post(vaccine.createVaccine);

    app.route('/vaccine/update/:vaccineId').put(vaccine.updateVaccine);

    app.route('/vaccine/get/:vaccineId').get(vaccine.getVaccine);

    app.route('/vaccine/getAll').get(vaccine.getVaccines);

    app.route('/vaccine/delete/:vaccineId').delete(vaccine.removeVaccineById);
};
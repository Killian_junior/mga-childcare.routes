/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var infection = require('../requesthandlers/child-care/child-care');

module.exports = function(app){

    app.route('/infection/create').post(infection.createInfection);

    app.route('/infection/update/:infectionId').put(infection.updateInfection);

    app.route('/infection/get/:infectionId').get(infection.getInfection);

    app.route('/infection/getAll').get(infection.getInfections);

    app.route('/infection/delete/:infectionId').delete(infection.removeInfectionById);
};
var child = require('../requesthandlers/child-care/child-care');

module.exports = function(app){

    app.route('/child/create').post(child.createChild);

    app.route('/child/update/:childId').put(child.updateChild);

    app.route('/child/get/:childId').get(child.getChild);

    app.route('/child/getAll').get(child.getChildren);

    app.route('/child/delete/:childId').delete(child.removeChildById);
};
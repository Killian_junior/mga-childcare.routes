/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var childVaccine = require('../requesthandlers/child-care/child-care');

module.exports = function(app){

    app.route('/child-vaccine/create').post(childVaccine.createChildVaccination);

    app.route('/child-vaccine/update/:child-vaccineId').put(childVaccine.updateChildVaccination);

    app.route('/child-vaccine/get/:child-vaccineId').get(childVaccine.getChildVaccination);

    app.route('/child-vaccine/getAll').get(childVaccine.getChildVaccinations);

    app.route('/child-vaccine/delete/:child-vaccineId').delete(childVaccine.removeChildVaccinationById);
};
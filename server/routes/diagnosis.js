/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var diagnosis = require('../requesthandlers/child-care/child-care');

module.exports = function(app){

    app.route('/diagnosis/create').post(diagnosis.createDiagnosis);

    app.route('/diagnosis/update/:diagnosisId').put(diagnosis.updateDiagnosis);

    app.route('/diagnosis/get/:diagnosisId').get(diagnosis.getDiagnosis);

    app.route('/diagnosis/getAll').get(diagnosis.getAllDiagnosis);

    app.route('/diagnosis/delete/:diagnosisId').delete(diagnosis.removeDiagnosisById);
};
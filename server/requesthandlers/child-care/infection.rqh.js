/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var BcServices = require('');
var InfectionObject = require('./infection.dto.js');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var infection = new InfectionObject(req.body);

    infection.created_by = req.user.id;

    BcServices.write.createInfection(infection, function(err, infection){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(infection);
        }
    });
};

exports.update = function(req, res){
    var data = new InfectionObject(req.body);

    BcServices.write.updateInfection(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var InfectionId = req.params.InfectionId;

    BcServices.write.removeInfectionById(InfectionId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var infectionId = req.params.infectionId;
    BcServices.read.getInfection(infectionId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var infections = req.params.infections;
    BcServices.read.getInfections(infections, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};
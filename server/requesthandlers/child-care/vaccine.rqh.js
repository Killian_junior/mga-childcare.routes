/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var BcServices = require('');
var VaccineObject = require('./vaccine.dto.js');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var vaccine = new VaccineObject(req.body);

    vaccine.created_by = req.user.id;

    BcServices.write.createVaccine(vaccine, function(err, vaccine){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(vaccine);
        }
    });
};

exports.update = function(req, res){
    var data = new VaccineObject(req.body);

    BcServices.write.updateVaccine(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var vaccineId = req.params.vaccineId;

    BcServices.write.removeVaccineById(vaccineId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var vaccineId = req.params.vaccineId;
    BcServices.read.getVaccine(vaccineId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var vaccines = req.params.vaccines;
    BcServices.read.getVaccines(vaccines, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};
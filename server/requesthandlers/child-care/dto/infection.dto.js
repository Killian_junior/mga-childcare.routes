/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var InfectionObject = function(args){

    var infection = {};

    infection.infection_details = args.infection_details;
    infection.remarks = args.remarks;
    infection.identification_date = args.identification_date;
    infection.primary_examiner = args.primary_examiner;
    infection.examiners = args.examiners;
    infection.management = args.management;
    infection.child = args.child;
    infection.created_by = args.created_by;
    infection.infection_classification = args.infection_classification;

    return infection;
};
module.exports = InfectionObject;
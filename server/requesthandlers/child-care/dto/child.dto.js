/**
 * Created by TERMINAL 7 on 10/21/2014.
 */

var ChildObject = function(args){

    var child = {};

    child.first_name = args.first_name;
    child.surname = args.surname;
    child.other_names = args.other_names;
    child.address  = args.address;
    child.gender  = args.gender;
    child.birthDate  = args.birthDate;
    child.location_info = args.location_info;
    child.birth_clinic_hospital = args.birth_clinic_hospital;
    child.birth_weight = args.birth_weight;
    child.primary_notification_email = args.primary_notification_email;
    child.primary_notification_phone = args.primary_notification_phone;
    child.parentInformation = args.parentInformation;
    child.birthInfections = args.birthInfections;
    child.vaccinations = args.vaccinations;
    child.renown_infections = args.renown_infections;
    child.diagnosis = args.diagnosis;
    child.infectionManagement = args.infectionManagement;
    child.created_by = args.created_by;
    child.required_vaccinations = args.required_vaccinations;

    return child;

};

module.exports = ChildObject;
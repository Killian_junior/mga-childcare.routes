/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var DiagnosisObject = function(args){
    var diagnosis = {};

    diagnosis.evaluation_details = args.evaluation_details;
    diagnosis.evaluation_date = args.evaluation_date;
    diagnosis.remarks = args.remarks;
    diagnosis.primary_examiner = args.primary_examiner;
    diagnosis.examiners = args.examiners;
    diagnosis.treatment = args.treatment;
    diagnosis.child = args.child;
    diagnosis.created_by = args.created_by;
};
module.exports = DiagnosisObject;
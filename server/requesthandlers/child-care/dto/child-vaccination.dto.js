/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var ChildImmunizationObject = function(args){

    var immunization = {};

    immunization.vaccine = args.vaccine;
    immunization.administration_date = args.administration_date;
    immunization.remarks = args.remarks;
    immunization.administration_by =  args.administration_by;
    immunization.child = args.child;
    immunization.created_by = args.created_by;

    return immunization;

};
module.exports = ChildImmunizationObject;
/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var VaccineObject = function(args){

    var vaccine = {};

    vaccine.vaccine_name = args.vaccine_name;
    vaccine.combat_disease = args.combat_disease;
    vaccine.required_time = args.required_time;
    vaccine.required_time_type = args.required_time_type;
    vaccine.remarks = args.remarks;
    vaccine.created_by = args.created_by;
    vaccine.discontinue = args.discontinue;
    vaccine.reason_for_discontinue = args.reason_for_discontinue;
};
module.exports = VaccineObject;
/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var BcServices = require('');
var DiagnosisObject = require('./diagnosis.dto.js');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var diagnosis = new DiagnosisObject(req.body);

    diagnosis.created_by = req.user.id;

    BcServices.write.createDiagnosis(diagnosis, function(err, diagnosis){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(diagnosis);
        }
    });
};

exports.update = function(req, res){
    var data = new DiagnosisObject(req.body);

    BcServices.write.updateDiagnosis(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var diagnosisId = req.params.diagnosisId;

    BcServices.write.removeDiagnosisById(diagnosisId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var diagnosisId = req.params.diagnosisId;
    BcServices.read.getDiagnosis(diagnosisId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var diagnosis = req.params.diagnosis;
    BcServices.read.getAllDiagnosis(diagnosis, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};
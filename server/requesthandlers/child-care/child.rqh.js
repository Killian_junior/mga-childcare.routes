var BcServices = require('');
var ChildObject = require('./child.dto.js');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var child = new ChildObject(req.body);

    child.created_by = req.user.id;

    BcServices.write.createChild(child, function(err, child){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(child);
        }
    });
};

exports.update = function(req, res){
    var data = new ChildObject(req.body);

    BcServices.write.updateChild(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var childId = req.params.childId;

    BcServices.write.removeChildById(childId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var childId = req.params.childId;
    BcServices.read.getChild(childId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var children = req.params.children;
    BcServices.read.getChildren(children, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};
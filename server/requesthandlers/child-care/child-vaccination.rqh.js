/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var BcServices = require('');
var ChildVaccinationObject = require('./child-vaccination.dto.js');
var errorHandler = require('../../config/errors');
_ = require('lodash');

exports.create = function(req, res){
    var childVaccination = new ChildVaccinationObject(req.body);

    childVaccination.created_by = req.user.id;

    BcServices.write.createChildVaccination(childVaccination, function(err, childVaccination){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(childVaccination);
        }
    });
};

exports.update = function(req, res){
    var data = new ChildVaccinationObject(req.body);

    BcServices.write.updateChildVaccination(data, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.remove = function(req, res){
    var childVaccinationId = req.params.childVaccinationId;

    BcServices.write.removeChildVaccinationById(childVaccinationId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(result);
        }
    });
};

exports.get = function(req, res){
    var childVaccinationId = req.params.childVaccinationId;
    BcServices.read.getChildVaccination(childVaccinationId, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }   else{
            res.jsonp(result);
        }
    });
};

exports.getAll = function(req, res){
    var childVaccinations = req.params.childVaccinations;
    BcServices.read.getChildVaccinations(childVaccinations, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else{
            res.jsonp(result);
        }
    });
};
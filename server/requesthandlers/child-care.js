/**
 * Created by TERMINAL 7 on 10/21/2014.
 */
var _ = require('lodash');

module.exports = _.extend(
    require('./child-care/child.rqh'),
    require('./child-care/child-vaccination.rqh'),
    require('./child-care/diagnosis.rqh'),
    require('./child-care/infection.rqh'),
    require('./child-care/vaccine.rqh')
);
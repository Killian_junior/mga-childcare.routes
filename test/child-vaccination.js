/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var ChildCareServices = require('../index'),
//    Account = require('./testmodels/accounts');
//    Vaccine = require('./testmodels/vaccine');
//    TestChild = require('./testmodels/test-child');
//
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Child Vaccination Specifications tests', function() {
//
//    var createdChildVaccination = {};
//
//    beforeEach(function (done) {
//        var account = new Account({
//            accountIdentity: 'Killian'
//        });
//        account.save();
//
//        var vaccine = new Vaccine({
//            vaccine: 'Killian'
//        });
//        vaccine.save();
//
//        var testChild = new TestChild({
//            testChildName: 'Killian'
//        });
//        testChild.save();
//
//
//        var childVaccination =  {
//
//            vaccine:vaccine._id,
//            administration_date:moment('12/06/2014', 'DD/MM/YYYY'),
//            remarks:'Beautiful',
//            child:testChild._id,
//            created_by: account._id
//
//
//
//
//        };
//        account.save(function(err, account){
//            if(account){
//                childVaccination.created_by = account._id;
//                ChildCareServices.write.createChildVaccination(childVaccination, function(err, result){
//                    if(err){
//                        console.log(err)
//                    }
//                    if(result){
//                        createdChildVaccination = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function(){
//        it('should run successfully', function(){
//            should.exist(createdChildVaccination._id);
//        });
//    });
//
//    afterEach(function(done){
//        ChildCareServices.write.removeChildVaccinationById(createdChildVaccination._id, function(err, result){
//            if(!err){
//                should.exist(result);
//            }
//            Account.remove().exec();
//            Vaccine.remove().exec();
//            TestChild.remove().exec();
//            done();
//        });
//    });
//
//});
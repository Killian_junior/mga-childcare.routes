/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VaccineSchema = new Schema({
    vaccine:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var Vaccine = mongoose.model('TestVaccine', VaccineSchema);

module.exports = Vaccine;
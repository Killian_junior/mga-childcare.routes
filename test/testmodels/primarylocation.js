/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PrimaryLocationSchema = new Schema({
    primaryLocationName:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var PrimaryLocation = mongoose.model('PrimaryLocation', PrimaryLocationSchema);

module.exports = PrimaryLocation;
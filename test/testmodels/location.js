/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LocationSchema = new Schema({
    location:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var Location = mongoose.model('Location', LocationSchema);

module.exports = Location;
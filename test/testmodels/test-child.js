/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TestChildSchema = new Schema({
    testChildName:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    }
});

var TestChild = mongoose.model('TestChild', TestChildSchema);

module.exports = TestChild;
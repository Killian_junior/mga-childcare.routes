/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AccountSchema = new Schema({
    accountIdentity:{
        type:String,
        default:'',
        required:'Please fill out this detail',
        trim:true
    },
    created:{
        type:Date,
        default:Date.now
    }
});

var Account = mongoose.model('Account', AccountSchema);

module.exports = Account;
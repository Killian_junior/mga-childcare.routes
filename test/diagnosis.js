/**
* Created by TERMINAL 7 on 10/20/2014.
*/
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var ChildCareServices = require('../index'),
//    Account = require('./testmodels/accounts');
//    TestChild = require('./testmodels/test-child');
//
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Diagnosis Specifications tests', function() {
//
//    var createdDiagnosis = {};
//
//    beforeEach(function (done) {
//        var account = new Account({
//            accountIdentity: 'Killian'
//        });
//        account.save();
//
//
//        var testChild = new TestChild({
//            testChildName: 'Killian'
//        });
//        testChild.save();
//
//
//
//        var diagnosis =  {
//
//            inception_Date:moment('10/10/2014', 'DD/MM/YYYY'),
//            evaluation_date:moment('12/06/2014', 'DD/MM/YYYY'),
//            remarks:'Beautiful',
//            child:testChild._id,
//            created_by: account._id
//
//
//
//
//        };
//        account.save(function(err, account){
//            if(account){
//                diagnosis.created_by = account._id;
//                ChildCareServices.write.createDiagnosis(diagnosis, function(err, result){
//                    if(err){
//                        console.log(err)
//                    }
//                    if(result){
//                        createdDiagnosis = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function(){
//        it('should run successfully', function(){
//            should.exist(createdDiagnosis._id);
//        });
//    });
//
//    afterEach(function(done){
//        ChildCareServices.write.removeDiagnosisById(createdDiagnosis._id, function(err, result){
//            if(!err){
//                should.exist(result);
//            }
//            Account.remove().exec();
//            TestChild.remove().exec();
//            done();
//        });
//    });
//
//});
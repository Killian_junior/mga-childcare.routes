/**
 * Created by TERMINAL 7 on 10/20/2014.
// */
var should = require('should');
var mongoose = require('mongoose');
var moment = require('moment');
var ChildCareServices = require('../index'),
    Account = require('./testmodels/accounts'),
    TestChild = require('./testmodels/test-child');


mongoose.connect('mongodb://localhost/hrdb');

describe('Vaccine Specifications tests', function() {

    var createdVaccine = {};

    beforeEach(function (done) {
        var account = new Account({
            accountIdentity: 'Killian'
        });
        account.save();

        var testChild = new TestChild({
            testChildName: 'Killian'
        });
        testChild.save();


        var Vaccine =  {



            vaccine_name:'HIV vaccine',
            required_time:'1800',
            required_time_type:'Days',
            created_by: account._id




        };
        account.save(function(err, account){
            if(account){
                Vaccine.created_by = account._id;
                ChildCareServices.write.createVaccine(Vaccine, function(err, result){
                    if(err){
                        console.log(err)
                    }
                    if(result){
                        createdVaccine = result;
                    }
                    done()
                });
            }
        });
    });

    describe('Method Save', function(){
        it('should run successfully', function(){
            should.exist(createdVaccine._id);
        });
    });

    afterEach(function(done){
        ChildCareServices.write.removeVaccineById(createdVaccine._id, function(err, result){
            if(!err){
                should.exist(result);
            }
            Account.remove().exec();
            TestChild.remove().exec();
            done();
        });
    });

});
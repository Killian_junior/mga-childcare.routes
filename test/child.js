/**
 * Created by TERMINAL 7 on 10/20/2014.
 */
//var should = require('should');
//var mongoose = require('mongoose');
//var moment = require('moment');
//var ChildCareServices = require('../index'),
//    Account = require('./testmodels/accounts');
//    Location = require('./testmodels/location');
//    PrimaryLocation = require('./testmodels/primarylocation');
//
//
//mongoose.connect('mongodb://localhost/hrdb');
//
//describe('Child Specifications tests', function() {
//
//    var createdChild = {};
//
//    beforeEach(function (done) {
//        var account = new Account({
//            accountIdentity: 'Killian'
//        });
//        account.save();
//        var location = new Location({
//            location: 'Akwa Ibom'
//        });
//        location.save();
//        var primaryLocation = new PrimaryLocation({
//            primaryLocationName: 'Uyo'
//        });
//        primaryLocation.save();
//
//
//        var child =  {
//
//            first_name:'Kenneth',
//            surname:'Thompson',
//            other_names:'Killian',
//            gender:'Male',
//            birthDate:moment('28/7/1992', 'DD/MM/YYYY'),
//            location_info:{
//                location_id:location._id,
//                primaryLocation_id:primaryLocation._id
//                           },
//            address:'34 Udoh street, Uyo',
//            created_by: account._id
//
//
//
//
//        };
//        account.save(function(err, account){
//            if(account){
//                child.created_by = account._id;
//                ChildCareServices.write.createChild(child, function(err, result){
//                    if(err){
//                        console.log(err)
//                    }
//                    if(result){
//                        createdChild = result;
//                    }
//                    done()
//                });
//            }
//        });
//    });
//
//    describe('Method Save', function(){
//        it('should run successfully', function(){
//            should.exist(createdChild._id);
//        });
//    });
//
//    afterEach(function(done){
//        ChildCareServices.write.removeChildById(createdChild._id, function(err, result){
//            if(!err){
//                should.exist(result);
//            }
//            Account.remove().exec();
//            Location.remove().exec();
//            PrimaryLocation.remove().exec();
//            done();
//        });
//    });
//
//});
/**
 * Created by Dominic on 15-Sep-2014.
 */

var _ = require('lodash');

module.exports.read = _.extend(
    require('./read_services/child'),
    require('./read_services/child-vaccination'),
    require('./read_services/diagnosis'),
    require('./read_services/infection'),
    require('./read_services/vaccine')

);



module.exports.write = _.extend(
    require('./write_services/child'),
    require('./write_services/child-vaccination'),
    require('./write_services/diagnosis'),
    require('./write_services/infection'),
    require('./write_services/vaccine')
);